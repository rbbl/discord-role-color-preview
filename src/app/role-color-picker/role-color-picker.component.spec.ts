import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleColorPickerComponent } from './role-color-picker.component';

describe('RoleColorPickerComponent', () => {
  let component: RoleColorPickerComponent;
  let fixture: ComponentFixture<RoleColorPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoleColorPickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
