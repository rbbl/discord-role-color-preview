import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {defaultColorPallet, hexColorPattern} from "../app.component";
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {ActivatedRoute, NavigationEnd, Params, Router} from "@angular/router";
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {Clipboard} from '@angular/cdk/clipboard';
import {LocationStrategy} from '@angular/common';
import {getColorsFromQueryParams} from "../main/main.component";
import {filter} from "rxjs";

@Component({
  selector: 'app-role-color-picker',
  templateUrl: './role-color-picker.component.html',
  styleUrls: ['./role-color-picker.component.scss']
})
export class RoleColorPickerComponent {
  private defaultColor = "#ffffff"

  @Output() colorUpdateEvent = new EventEmitter<string[]>();
  @Input() colors: string[] = defaultColorPallet

  @ViewChild('colorList') colorListElement: ElementRef<HTMLInputElement> | null = null;

  choseColorForm = this.formBuilder.group({
    color: new FormControl(this.defaultColor, [Validators.pattern(hexColorPattern)])
  });

  constructor(private formBuilder: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
              private location: LocationStrategy, private clipboard: Clipboard) {
    router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      let loadedColors = getColorsFromQueryParams(activatedRoute)
      if (loadedColors.length != 0) {
        this.colors = loadedColors
      }
      this.colorUpdateEvent.emit(this.colors)
    })
  }

  onSubmit() {
    let color: string = this.choseColorForm.value.color
    if (!color.startsWith("#")) {
      color = "#" + color;
    }
    this.colors.push(color);
    this.updateUrl()
  }

  updateUrl() {
    const queryParams: Params = {};
    this.colors.forEach((value, index) => {
      queryParams["color" + index] = value.replace("#", "")
    })
    let preNavColors = this.colors.slice();
    this.router.navigate([], {relativeTo: this.activatedRoute, queryParams: queryParams}).then(() => {
      this.colors = preNavColors //need to reset here because else the value duplicate for some reason
      this.colorUpdateEvent.emit(this.colors)
    })
  }

  fixColor() {
    let color: string = this.choseColorForm.value.color
    if (!color.startsWith("#")) {
      this.choseColorForm.value.color = "#" + color;
    }
  }

  deleteColorAtIndex(index: number) {
    this.colors.splice(index, 1);
    this.updateUrl()
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.colors, event.previousIndex, event.currentIndex);
    this.updateUrl();
  }

  copyColorToClipboard(index: number) {
    this.clipboard.copy(this.colors[index])
    let copiedLabel = this.colorListElement?.nativeElement.querySelector("li.color-element:nth-child(" + (index + 1) + ") > span:last-of-type")
    if (copiedLabel) {
      copiedLabel.classList.add("animate");
      copiedLabel.addEventListener("animationend", () => copiedLabel?.classList.remove('animate'))
    }
  }
}
