import {Component, Input, OnInit} from '@angular/core';
import {ChatPreviewType} from "./ChatPreviewType";

@Component({
  selector: 'app-chat-preview',
  templateUrl: './chat-preview.component.html',
  styleUrls: ['./chat-preview.component.scss']
})
export class ChatPreviewComponent implements OnInit {

  @Input() type: ChatPreviewType = ChatPreviewType.DARK
  @Input() colors: string[] = []

  constructor() { }

  ngOnInit(): void {
  }
}
