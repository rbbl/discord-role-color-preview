import {Component, Input, OnInit} from '@angular/core';
import {ChatPreviewType} from "../chat-preview/ChatPreviewType";

@Component({
  selector: 'app-message-preview',
  templateUrl: './message-preview.component.html',
  styleUrls: ['./message-preview.component.scss']
})
export class MessagePreviewComponent implements OnInit {

  @Input() type: ChatPreviewType = ChatPreviewType.DARK
  @Input() color: string | null = null

  constructor() {
  }

  ngOnInit(): void {
  }
}
