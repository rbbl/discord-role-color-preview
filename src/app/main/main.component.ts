import {Component, OnInit} from '@angular/core';
import {ChatPreviewType} from "../chat-preview/ChatPreviewType";
import {defaultColorPallet, hexColorPattern} from "../app.component";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  title = 'discord-role-color-preview';
  colors: string[] = defaultColorPallet
  lightType = ChatPreviewType.LIGHT
  darkType = ChatPreviewType.DARK

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    let loadedColors = getColorsFromQueryParams(this.route)
    if (loadedColors.length != 0) {
      this.colors = loadedColors
    }
  }
}

export function getColorsFromQueryParams(route: ActivatedRoute): string[] {
  let loadedColors: string[] = []
  route.queryParamMap.subscribe(params => {
    for (let i = 0; i <= 100; i++) {
      if (!params.has("color" + i)) {
        break;
      }
      let value = params.get("color" + i) as string
      if (value.match(hexColorPattern) != null) {
        if (value.startsWith("#")) {
          loadedColors.push(value)
        } else {
          loadedColors.push("#" + value)
        }
      }
    }
  });
  return loadedColors
}
