import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}

export let defaultColorPallet: string[] = ["#003049", "#D62828", "#F77F00", "#FCBF49", "#EAE2B7"]
export let hexColorPattern = new RegExp("^#?[0-9a-f]{6}$", "i")
