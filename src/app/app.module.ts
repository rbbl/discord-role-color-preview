import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {ChatPreviewComponent} from './chat-preview/chat-preview.component';
import {MessagePreviewComponent} from './message-preview/message-preview.component';
import {RoleColorPickerComponent} from './role-color-picker/role-color-picker.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AppRoutingModule} from "./app-routing.module";
import {MainComponent} from "./main/main.component";
import {DragDropModule} from "@angular/cdk/drag-drop";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatPreviewComponent,
    MessagePreviewComponent,
    RoleColorPickerComponent,
    MainComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
