FROM node:18 as builder
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
WORKDIR /app
RUN npm ci
COPY src/ /app/src
COPY angular.json /app/angular.json
COPY tsconfig.app.json /app/tsconfig.app.json
COPY tsconfig.json /app/tsconfig.json
COPY tsconfig.spec.json /app/tsconfig.spec.json
RUN npx -p @angular/cli ng build --configuration production

FROM nginx:1
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/dist/discord-role-color-preview /usr/share/nginx/html
